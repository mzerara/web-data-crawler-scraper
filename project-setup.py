# Dependencies
import os
import logging
import importlib.util
import ast


# Environment variables:
sourceDirectoryName = os.environ['SOURCE_DIRECTORY_NAME']
keepContainerRunning = ast.literal_eval(os.environ['KEEP_CONTAINER_RUNNING'])
spiderName = "my_spider"


# Create source directory:
if not os.path.isdir(sourceDirectoryName):
    logging.warning("\n********** Creating a source directory **********\n")
    os.mkdir(sourceDirectoryName)


# List of commands:
commands = [
    # {
    #     "task": "install_package",
    #     "message": "Installing packages",
    #     "cmd": "pipenv install ***"
    # },
    {
        "task": "run_crawler",
        "message": "Crawling website",
        "cmd": "cd src && pipenv run scrapy crawl {}".format(spiderName)
    },
    # {
    #     "task": "keep_container_running",
    #     "message": "Container running",
    #     "cmd": "tail -f /dev/null"
    # },

]


# Execution:
for command in commands:

    # Set default command execution to true:
    executeCommand = True

    # List of tasks to execute:
    if not keepContainerRunning:
        executeCommand = False

    # Execute command if needed:
    if executeCommand:
        logging.warning(
            "\n********** {} **********\n".format(command["message"]))
        os.system(command["cmd"])
