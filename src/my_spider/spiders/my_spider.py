import os
import json
import scrapy
# from bs4 import BeautifulSoup
from scrapy.spiders import CrawlSpider, Rule
from scrapy.linkextractors import LinkExtractor


# read spider-configs
with open("./spider.config.json", 'r') as json_file:
    spiderConfigs = json.loads(json_file.read())


# Extract Required items list
requiredItems = []
for component in spiderConfigs["articleComponents"]:
    if component["required"]:
        requiredItems.append(component["item"])


class websiteCrawler(CrawlSpider):

    name = spiderConfigs["spiderName"]
    allowed_domains = spiderConfigs["allowedDomains"]
    start_urls = spiderConfigs["startUrls"]

    rules = (
        Rule(
            LinkExtractor(),
            callback='parse_item',
            follow=True
        ),
    )

    def parse_item(self, response):

        if response.status == 404:
            self.logger.info(
                '********** Ignoring: %s **********', response.url)
        else:
            self.logger.info(
                '********** Scrapping data from: %s **********', response.url)

            webpage = {
                "url": None,
                "extractedItems": list(),
                "articleComponents": list()
            }

            for component in spiderConfigs["articleComponents"]:
                if component["cssSelector"]:
                    html = response.css(
                        component["cssSelector"]).extract_first()
                    if html:
                        webpage["articleComponents"].append(
                            {"item": component["item"], "html": html})
                        # text = BeautifulSoup(html).get_text().strip()
                        if component["required"]:
                            webpage["extractedItems"].append(component["item"])

            if webpage["extractedItems"] == requiredItems:
                self.logger.info(
                    '********** Storing data in the database: %s **********', response.url)
                webpage["url"] = response.url

                self.logger.info(
                    '********** Storing data in the database: %s **********', webpage)
                # Store in db
                # url as pk
                # tag
